﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;

namespace Shedinger2
{
    public partial class Form2 : Form
    {
        int N, M;
        int idxT, idxZ;
        double freq, R;
        Complex[][] psi;
        Complex[][] spectre;
        Complex[] for_draw;
        double pi = Math.PI;

        // Быстрое фурье преобразование
        // type = -1 - прямое преобразование
        // type = 1 - обратное преобразование
        void fft(long v_size, Complex[] F, double type)
        {
            Complex temp, w, c;
            long i, i1, j, j1, istep;
            long m, mmax;
            long n = v_size;
            double fn, r1, theta;
            fn = (double)n;
            double r = pi * type;
            j = 1;
            for (i = 1; i <= n; i++)
            {
                i1 = i - 1;
                if (i < j)
                {
                    j1 = j - 1;
                    temp = F[j1];
                    F[j1] = F[i1];
                    F[i1] = temp;
                }
                m = n / 2;
                while (j > m) { j -= m; m = (m + 1) / 2; }
                j += m;
            }
            mmax = 1;
            while (mmax < n)
            {
                istep = 2 * mmax;
                r1 = r / (double)mmax;
                for (m = 1; m <= mmax; m++)
                {
                    theta = r1 * (double)(m - 1);
                    w = new Complex(Math.Cos(theta), Math.Sin(theta));
                    for (i = m - 1; i < n; i += istep)
                    {
                        j = i + mmax;
                        c = F[j];
                        temp = w * c;
                        F[j] = F[i] - temp;
                        F[i] = F[i] + temp;
                    }
                }
                mmax = istep;
            }
            if (type > 0)
                for (i = 0; i < n; i++)
                {
                    Complex buf = new Complex(F[i].Real / fn, F[i].Imaginary / fn);
                    F[i] = buf;
                }
            return;
        }

        public Form2(int n, int m, int idx, double dt, double r, Complex[][] arr)
        {
            InitializeComponent();
            N = n;
            M = m;
            idxZ = idx;
            freq = 1.0 / dt;
            R = r;
            psi = arr;

            TrackT.Minimum = 0;
            TrackT.Maximum = N;
            TrackT.TickFrequency = 1;
            TrackT.Value = 0;
            TrackT.TickStyle = TickStyle.None;

            TrackZ.Minimum = 1;
            TrackZ.Maximum = M - 1;
            TrackZ.TickFrequency = 1;
            TrackZ.Value = idxZ;
            TrackZ.TickStyle = TickStyle.None;

            textBox13.Text = String.Format("Выбор отчета t, текущее значение: {0}", TrackT.Value);
            textBox12.Text = String.Format("Выбор индекса, текущее значение: {0}", TrackZ.Value);

            idxT = (int)TrackT.Value;
            idxZ = (int)TrackZ.Value;

            spectre = new Complex[M + 1][];
            for (int i = 0; i <= M; i++)
                spectre[i] = new Complex[N];

            for (int i = 0; i < N; i++)
                for (int j = 0; j <= M; j++)
                {
                    spectre[j][i] = psi[i][j];
                }

            for (int i = 0; i <= M; i++)
            {
                fft(N, spectre[i], -1);
            }

            draw_spectre();
        }

        double f(int i)
        {
            return (double)i / (double)N * freq;
        }

        // Поиск минимального и максимального значений массива
        void find_max_min(Complex[] arr, int size, ref double max, ref double min)
        {
            for (int i = 0; i < size; i++)
            {
                if (arr[i].Magnitude > max) max = arr[i].Magnitude;
                if (arr[i].Magnitude < min) min = arr[i].Magnitude;
            }
        }

        void draw(Complex[] arr, int size, double minX, double maxX)
        {
            // Очистка графиков
            Graph.Series["Спектр"].Points.Clear();

            double minY = 0, maxY = 0;
            find_max_min(arr, size, ref maxY, ref minY);

            // Настройка области рисования
            Graph.ChartAreas[0].AxisX.Minimum = minX;
            Graph.ChartAreas[0].AxisX.Maximum = maxX;
            Graph.ChartAreas[0].AxisX.Interval = (maxX - minX) / 8;
            Graph.ChartAreas[0].AxisY.Minimum = minY;
            Graph.ChartAreas[0].AxisY.Maximum = maxY;
            Graph.ChartAreas[0].AxisY.Interval = (maxY - minY) / 8;

            Graph.ChartAreas[0].CursorX.IsUserEnabled = true;
            Graph.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;
            Graph.ChartAreas[0].AxisX.ScaleView.Zoomable = true;
            Graph.ChartAreas[0].AxisX.ScrollBar.IsPositionedInside = true;

            Graph.ChartAreas[0].CursorY.IsUserEnabled = true;
            Graph.ChartAreas[0].CursorY.IsUserSelectionEnabled = true;
            Graph.ChartAreas[0].AxisY.ScaleView.Zoomable = true;
            Graph.ChartAreas[0].AxisY.ScrollBar.IsPositionedInside = true;

            // Oтрисовка графика
            for (int i = 0; i < size; i++)
            {
                Graph.Series["Спектр"].Points.AddXY(f(i) + minX, arr[i].Magnitude);
            }
            draw_line(idxT, 0, freq);
        }

        void draw_line(int idx, double minX, double maxX)
        {
            Graph.Series["Номер отчета"].Points.Clear();
            Graph.Series["Номер отчета"].Points.AddXY(f(idx) + minX, -1e4);
            Graph.Series["Номер отчета"].Points.AddXY(f(idx) + minX, 1e4);
        }

        private void TrackT_Scroll(object sender, EventArgs e)
        {
            textBox13.Text = String.Format("Выбор отчета t, текущее значение: {0}", TrackT.Value);
            idxT = (int)TrackT.Value;
            draw_line(idxT, 0, freq);
        }

        private void TrackZ_Scroll(object sender, EventArgs e)
        {
            textBox12.Text = String.Format("Выбор индекса, текущее значение: {0}", TrackZ.Value);
            idxZ = (int)TrackZ.Value;
            draw_spectre();
        }

        private void draw_spectre()
        {
            for_draw = new Complex[N];
            for (int i = 0; i < N; i++)
                for_draw[i] = psi[i][idxZ];
            fft(N, for_draw, -1);
            draw(for_draw, N, 0, freq);
        }

        private void DrawWF_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            return_WF();
        }

        public Complex[] return_WF()
        {
            Complex[] arr = new Complex[M + 1];
            for (int i = 0; i <= M; i++)
            {
                arr[i] = spectre[i][idxT];
            }
            return arr;
        }

        public int return_idx()
        {
            return idxZ;
        }
    }
}
