﻿namespace Shedinger2
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.Graph = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.TrackZ = new System.Windows.Forms.TrackBar();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.TrackT = new System.Windows.Forms.TrackBar();
            this.DrawWF = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Graph)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackT)).BeginInit();
            this.SuspendLayout();
            // 
            // Graph
            // 
            chartArea1.Name = "ChartArea1";
            this.Graph.ChartAreas.Add(chartArea1);
            legend1.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend1.Name = "Legend1";
            this.Graph.Legends.Add(legend1);
            this.Graph.Location = new System.Drawing.Point(-2, -2);
            this.Graph.Margin = new System.Windows.Forms.Padding(2);
            this.Graph.Name = "Graph";
            series1.BorderWidth = 3;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series1.Color = System.Drawing.Color.Navy;
            series1.Legend = "Legend1";
            series1.Name = "Спектр";
            series2.BorderWidth = 2;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Color = System.Drawing.Color.Maroon;
            series2.Legend = "Legend1";
            series2.Name = "Номер отчета";
            this.Graph.Series.Add(series1);
            this.Graph.Series.Add(series2);
            this.Graph.Size = new System.Drawing.Size(738, 423);
            this.Graph.TabIndex = 0;
            this.Graph.Text = "chart1";
            // 
            // textBox13
            // 
            this.textBox13.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox13.Location = new System.Drawing.Point(297, 424);
            this.textBox13.Name = "textBox13";
            this.textBox13.ReadOnly = true;
            this.textBox13.Size = new System.Drawing.Size(278, 13);
            this.textBox13.TabIndex = 21;
            this.textBox13.Text = "Выбор отчета времени";
            // 
            // TrackZ
            // 
            this.TrackZ.Location = new System.Drawing.Point(-1, 443);
            this.TrackZ.Name = "TrackZ";
            this.TrackZ.Size = new System.Drawing.Size(278, 45);
            this.TrackZ.TabIndex = 20;
            this.TrackZ.Scroll += new System.EventHandler(this.TrackZ_Scroll);
            // 
            // textBox12
            // 
            this.textBox12.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox12.Location = new System.Drawing.Point(7, 424);
            this.textBox12.Name = "textBox12";
            this.textBox12.ReadOnly = true;
            this.textBox12.Size = new System.Drawing.Size(290, 13);
            this.textBox12.TabIndex = 19;
            this.textBox12.Text = "Выбор индекса";
            // 
            // TrackT
            // 
            this.TrackT.Location = new System.Drawing.Point(289, 443);
            this.TrackT.Name = "TrackT";
            this.TrackT.Size = new System.Drawing.Size(278, 45);
            this.TrackT.TabIndex = 18;
            this.TrackT.Scroll += new System.EventHandler(this.TrackT_Scroll);
            // 
            // DrawWF
            // 
            this.DrawWF.Location = new System.Drawing.Point(589, 454);
            this.DrawWF.Margin = new System.Windows.Forms.Padding(2);
            this.DrawWF.Name = "DrawWF";
            this.DrawWF.Size = new System.Drawing.Size(139, 22);
            this.DrawWF.TabIndex = 23;
            this.DrawWF.Text = "Отрисовать ВФ";
            this.DrawWF.UseVisualStyleBackColor = true;
            this.DrawWF.Click += new System.EventHandler(this.DrawWF_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 488);
            this.Controls.Add(this.DrawWF);
            this.Controls.Add(this.textBox13);
            this.Controls.Add(this.TrackZ);
            this.Controls.Add(this.textBox12);
            this.Controls.Add(this.TrackT);
            this.Controls.Add(this.Graph);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form2";
            ((System.ComponentModel.ISupportInitialize)(this.Graph)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackT)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart Graph;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TrackBar TrackZ;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TrackBar TrackT;
        private System.Windows.Forms.Button DrawWF;
    }
}