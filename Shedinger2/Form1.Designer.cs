﻿namespace Shedinger2
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.Graph = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.NTB = new System.Windows.Forms.TextBox();
            this.dispTB = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.V0TB = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.mat_xTB = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.EnergyTB = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.RTB = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.MTB = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.StartTimerBT = new System.Windows.Forms.Button();
            this.WritingForFourierBT = new System.Windows.Forms.Button();
            this.GetSpectreBT = new System.Windows.Forms.Button();
            this.WFdispTB = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.WFmat_xTB = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.InitializeBT = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.energyVF_TB = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.dtTB = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.MainTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Graph)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // Graph
            // 
            chartArea1.Name = "ChartArea1";
            this.Graph.ChartAreas.Add(chartArea1);
            legend1.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend1.Name = "Legend1";
            this.Graph.Legends.Add(legend1);
            this.Graph.Location = new System.Drawing.Point(-1, -1);
            this.Graph.Name = "Graph";
            series1.BorderColor = System.Drawing.Color.DarkBlue;
            series1.BorderWidth = 3;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series1.Color = System.Drawing.Color.Navy;
            series1.Legend = "Legend1";
            series1.Name = "ВФ";
            series2.BorderWidth = 5;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series2.Color = System.Drawing.Color.Red;
            series2.LabelBorderWidth = 3;
            series2.Legend = "Legend1";
            series2.MarkerSize = 10;
            series2.Name = "Яма";
            this.Graph.Series.Add(series1);
            this.Graph.Series.Add(series2);
            this.Graph.Size = new System.Drawing.Size(738, 416);
            this.Graph.TabIndex = 0;
            this.Graph.Text = "chart1";
            this.Graph.Click += new System.EventHandler(this.Graph_Click);
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Location = new System.Drawing.Point(6, 47);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(61, 39);
            this.textBox1.TabIndex = 1;
            this.textBox1.Text = "Отчеты по времени:";
            // 
            // NTB
            // 
            this.NTB.Location = new System.Drawing.Point(82, 47);
            this.NTB.Name = "NTB";
            this.NTB.Size = new System.Drawing.Size(52, 20);
            this.NTB.TabIndex = 2;
            this.NTB.Text = "512";
            // 
            // dispTB
            // 
            this.dispTB.Location = new System.Drawing.Point(108, 80);
            this.dispTB.Name = "dispTB";
            this.dispTB.Size = new System.Drawing.Size(52, 20);
            this.dispTB.TabIndex = 4;
            this.dispTB.Text = "0,01";
            // 
            // textBox3
            // 
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.Location = new System.Drawing.Point(6, 83);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(81, 20);
            this.textBox3.TabIndex = 3;
            this.textBox3.Text = "Дисперсия: ";
            // 
            // V0TB
            // 
            this.V0TB.Location = new System.Drawing.Point(108, 19);
            this.V0TB.Name = "V0TB";
            this.V0TB.Size = new System.Drawing.Size(52, 20);
            this.V0TB.TabIndex = 6;
            this.V0TB.Text = "2";
            // 
            // textBox5
            // 
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox5.Location = new System.Drawing.Point(6, 19);
            this.textBox5.Multiline = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(70, 20);
            this.textBox5.TabIndex = 5;
            this.textBox5.Text = "Амплитуда: ";
            // 
            // mat_xTB
            // 
            this.mat_xTB.Location = new System.Drawing.Point(108, 42);
            this.mat_xTB.Name = "mat_xTB";
            this.mat_xTB.Size = new System.Drawing.Size(52, 20);
            this.mat_xTB.TabIndex = 8;
            this.mat_xTB.Text = "0";
            // 
            // textBox7
            // 
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox7.Location = new System.Drawing.Point(6, 45);
            this.textBox7.Multiline = true;
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(96, 32);
            this.textBox7.TabIndex = 7;
            this.textBox7.Text = "Математическое ожидание :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.mat_xTB);
            this.groupBox1.Controls.Add(this.dispTB);
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.V0TB);
            this.groupBox1.Controls.Add(this.textBox5);
            this.groupBox1.Controls.Add(this.textBox7);
            this.groupBox1.Location = new System.Drawing.Point(743, -1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(237, 112);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Параметры барьера";
            // 
            // EnergyTB
            // 
            this.EnergyTB.Location = new System.Drawing.Point(82, 17);
            this.EnergyTB.Name = "EnergyTB";
            this.EnergyTB.Size = new System.Drawing.Size(52, 20);
            this.EnergyTB.TabIndex = 11;
            this.EnergyTB.Text = "20";
            // 
            // textBox9
            // 
            this.textBox9.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox9.Location = new System.Drawing.Point(6, 17);
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.Size = new System.Drawing.Size(52, 13);
            this.textBox9.TabIndex = 10;
            this.textBox9.Text = "Энергия";
            // 
            // RTB
            // 
            this.RTB.Location = new System.Drawing.Point(82, 111);
            this.RTB.Name = "RTB";
            this.RTB.Size = new System.Drawing.Size(52, 20);
            this.RTB.TabIndex = 13;
            this.RTB.Text = "2";
            // 
            // textBox11
            // 
            this.textBox11.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox11.Location = new System.Drawing.Point(6, 111);
            this.textBox11.Multiline = true;
            this.textBox11.Name = "textBox11";
            this.textBox11.ReadOnly = true;
            this.textBox11.Size = new System.Drawing.Size(78, 39);
            this.textBox11.TabIndex = 12;
            this.textBox11.Text = "Граничные условия: ";
            // 
            // MTB
            // 
            this.MTB.Location = new System.Drawing.Point(82, 80);
            this.MTB.Name = "MTB";
            this.MTB.Size = new System.Drawing.Size(52, 20);
            this.MTB.TabIndex = 19;
            this.MTB.Text = "256";
            // 
            // textBox15
            // 
            this.textBox15.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox15.Location = new System.Drawing.Point(6, 78);
            this.textBox15.Multiline = true;
            this.textBox15.Name = "textBox15";
            this.textBox15.ReadOnly = true;
            this.textBox15.Size = new System.Drawing.Size(81, 34);
            this.textBox15.TabIndex = 18;
            this.textBox15.Text = "Отчеты в пространстве:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.MTB);
            this.groupBox2.Controls.Add(this.textBox15);
            this.groupBox2.Controls.Add(this.RTB);
            this.groupBox2.Controls.Add(this.textBox11);
            this.groupBox2.Controls.Add(this.EnergyTB);
            this.groupBox2.Controls.Add(this.textBox9);
            this.groupBox2.Controls.Add(this.NTB);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Location = new System.Drawing.Point(743, 265);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(206, 150);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Параметры для решения задачи";
            // 
            // StartTimerBT
            // 
            this.StartTimerBT.Enabled = false;
            this.StartTimerBT.Location = new System.Drawing.Point(212, 376);
            this.StartTimerBT.Name = "StartTimerBT";
            this.StartTimerBT.Size = new System.Drawing.Size(125, 22);
            this.StartTimerBT.TabIndex = 21;
            this.StartTimerBT.Text = "Запуск таймера";
            this.StartTimerBT.UseVisualStyleBackColor = true;
            this.StartTimerBT.Click += new System.EventHandler(this.StartTimerBT_Click);
            // 
            // WritingForFourierBT
            // 
            this.WritingForFourierBT.Enabled = false;
            this.WritingForFourierBT.Location = new System.Drawing.Point(343, 376);
            this.WritingForFourierBT.Name = "WritingForFourierBT";
            this.WritingForFourierBT.Size = new System.Drawing.Size(126, 22);
            this.WritingForFourierBT.TabIndex = 22;
            this.WritingForFourierBT.Text = "Отчеты и Фурье";
            this.WritingForFourierBT.UseVisualStyleBackColor = true;
            this.WritingForFourierBT.Click += new System.EventHandler(this.WritingForFourierBT_Click);
            // 
            // GetSpectreBT
            // 
            this.GetSpectreBT.Enabled = false;
            this.GetSpectreBT.Location = new System.Drawing.Point(475, 376);
            this.GetSpectreBT.Name = "GetSpectreBT";
            this.GetSpectreBT.Size = new System.Drawing.Size(104, 22);
            this.GetSpectreBT.TabIndex = 23;
            this.GetSpectreBT.Text = "Спектр";
            this.GetSpectreBT.UseVisualStyleBackColor = true;
            this.GetSpectreBT.Click += new System.EventHandler(this.GetSpectreBT_Click);
            // 
            // WFdispTB
            // 
            this.WFdispTB.Location = new System.Drawing.Point(119, 20);
            this.WFdispTB.Name = "WFdispTB";
            this.WFdispTB.Size = new System.Drawing.Size(52, 20);
            this.WFdispTB.TabIndex = 25;
            this.WFdispTB.Text = "0,1";
            // 
            // textBox6
            // 
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox6.Location = new System.Drawing.Point(8, 20);
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(82, 13);
            this.textBox6.TabIndex = 24;
            this.textBox6.Text = "Дисперсия: ";
            // 
            // WFmat_xTB
            // 
            this.WFmat_xTB.Location = new System.Drawing.Point(119, 43);
            this.WFmat_xTB.Name = "WFmat_xTB";
            this.WFmat_xTB.Size = new System.Drawing.Size(52, 20);
            this.WFmat_xTB.TabIndex = 27;
            this.WFmat_xTB.Text = "0";
            // 
            // textBox4
            // 
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox4.Location = new System.Drawing.Point(6, 46);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(113, 13);
            this.textBox4.TabIndex = 26;
            this.textBox4.Text = " Среднее положение :";
            // 
            // InitializeBT
            // 
            this.InitializeBT.Location = new System.Drawing.Point(78, 376);
            this.InitializeBT.Name = "InitializeBT";
            this.InitializeBT.Size = new System.Drawing.Size(128, 22);
            this.InitializeBT.TabIndex = 28;
            this.InitializeBT.Text = "Инициализация";
            this.InitializeBT.UseVisualStyleBackColor = true;
            this.InitializeBT.Click += new System.EventHandler(this.InitializeBT_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.energyVF_TB);
            this.groupBox3.Controls.Add(this.textBox2);
            this.groupBox3.Controls.Add(this.dtTB);
            this.groupBox3.Controls.Add(this.textBox8);
            this.groupBox3.Controls.Add(this.WFmat_xTB);
            this.groupBox3.Controls.Add(this.textBox4);
            this.groupBox3.Controls.Add(this.WFdispTB);
            this.groupBox3.Controls.Add(this.textBox6);
            this.groupBox3.Location = new System.Drawing.Point(743, 116);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(237, 144);
            this.groupBox3.TabIndex = 30;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Параметры ВФ";
            // 
            // energyVF_TB
            // 
            this.energyVF_TB.Location = new System.Drawing.Point(119, 97);
            this.energyVF_TB.Name = "energyVF_TB";
            this.energyVF_TB.Size = new System.Drawing.Size(52, 20);
            this.energyVF_TB.TabIndex = 31;
            this.energyVF_TB.Text = "0,0";
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Location = new System.Drawing.Point(6, 100);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(61, 20);
            this.textBox2.TabIndex = 30;
            this.textBox2.Text = "Энергия: ";
            // 
            // dtTB
            // 
            this.dtTB.Location = new System.Drawing.Point(119, 74);
            this.dtTB.Name = "dtTB";
            this.dtTB.Size = new System.Drawing.Size(52, 20);
            this.dtTB.TabIndex = 29;
            this.dtTB.Text = "0,01";
            // 
            // textBox8
            // 
            this.textBox8.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox8.Location = new System.Drawing.Point(8, 74);
            this.textBox8.Multiline = true;
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(97, 20);
            this.textBox8.TabIndex = 28;
            this.textBox8.Text = "Шаг по времени: ";
            this.textBox8.TextChanged += new System.EventHandler(this.textBox8_TextChanged);
            // 
            // MainTimer
            // 
            this.MainTimer.Interval = 75;
            this.MainTimer.Tick += new System.EventHandler(this.MainTimer_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(930, 422);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.InitializeBT);
            this.Controls.Add(this.GetSpectreBT);
            this.Controls.Add(this.WritingForFourierBT);
            this.Controls.Add(this.StartTimerBT);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Graph);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Graph)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart Graph;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox NTB;
        private System.Windows.Forms.TextBox dispTB;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox V0TB;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox mat_xTB;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox EnergyTB;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox RTB;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox MTB;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button StartTimerBT;
        private System.Windows.Forms.Button WritingForFourierBT;
        private System.Windows.Forms.Button GetSpectreBT;
        private System.Windows.Forms.TextBox WFdispTB;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox WFmat_xTB;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button InitializeBT;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Timer MainTimer;
        private System.Windows.Forms.TextBox dtTB;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox energyVF_TB;
        private System.Windows.Forms.TextBox textBox2;
    }
}

