﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;


namespace Shedinger2
{
    public partial class Form1 : Form
    {
        // Переменные
        Complex ii = new Complex(0, 1);
        public int N, M, idx, idxZ;
        public double R, E, dz, dt;
        public double WFdisp, WFmat_x, WFamp;
        public double disp, mat_x, V0;
        public Complex[][] psi;
        public Complex[] temp_psi;
        public bool first = true;
        public bool start = true;
        public bool Fourier = false;
  


        public Form1()
        {
            InitializeComponent();
        }

        // Поиск минимального и максимального значений массива
        void find_max_min(Complex[] arr, int size, ref double max, ref double min, double type)
        {
            for (int i = 0; i < size; i++)
            {
                if (type == 0)
                {
                    if (arr[i].Real > max) max = arr[i].Real;
                    if (arr[i].Real < min) min = arr[i].Real;
                }
                if (type == 1)
                {
                    if (arr[i].Magnitude > max) max = arr[i].Magnitude;
                    if (arr[i].Magnitude < min) min = arr[i].Magnitude;
                }
                if (type == 2)
                {
                    if (arr[i].Imaginary > max) max = arr[i].Imaginary;
                    if (arr[i].Imaginary < min) min = arr[i].Imaginary;
                }
            }
        }

        // Подсчет энергии пакета
        double get_energy(Complex[] arr, int size)
        {
            double energy = 0;
            for (int i = 0; i < size; i++)
            {
                energy += arr[i].Magnitude * dz;
            }
            return energy;
        }

        // Отрисовка массива значений
        void draw(Complex[] arr, int size, double minX, double maxX, double type)
        {
            // Очистка графиков
            Graph.Series[0].Name = String.Format("ВФ");
            energyVF_TB.Text = Convert.ToString(get_energy(arr, size));
            Graph.Series[0].Points.Clear();

            double minY = 0, maxY = 0;

                first = false;
                find_max_min(arr, size, ref maxY, ref minY, type);
            

            // Настройка области рисования
            Graph.ChartAreas[0].AxisX.Minimum = minX;
            Graph.ChartAreas[0].AxisX.Maximum = maxX;
            Graph.ChartAreas[0].AxisX.Interval = (maxX - minX) / 8;
            Graph.ChartAreas[0].AxisY.Minimum = minY;
            Graph.ChartAreas[0].AxisY.Maximum = maxY;
            Graph.ChartAreas[0].AxisY.Interval = (maxY - minY) / 8;

            Graph.ChartAreas[0].CursorX.IsUserEnabled = true;
            Graph.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;
            Graph.ChartAreas[0].AxisX.ScaleView.Zoomable = true;
            Graph.ChartAreas[0].AxisX.ScrollBar.IsPositionedInside = true;

            Graph.ChartAreas[0].CursorY.IsUserEnabled = true;
            Graph.ChartAreas[0].CursorY.IsUserSelectionEnabled = true;
            Graph.ChartAreas[0].AxisY.ScaleView.Zoomable = true;
            Graph.ChartAreas[0].AxisY.ScrollBar.IsPositionedInside = true;

            // отрисовка графика
            for (int i = 0; i < size; i++)
            {
                double x = (maxX - minX) / (double)(size - 1) * (double)i + minX;
                if (type == 0)
                    Graph.Series[0].Points.AddXY(x, arr[i].Real);
                if (type == 1)
                    Graph.Series[0].Points.AddXY(x, arr[i].Magnitude);
                if (type == 2)
                    Graph.Series[0].Points.AddXY(x, arr[i].Imaginary);
            }
        }

        double ampl;
        // Энергия в текущей координате
        double u(double z)
        {

            ampl = (-1) * V0;
           return ampl * (Math.Exp(-(z - mat_x) * (z - mat_x) / 2 / disp / disp));// + Math.Exp(-(z + mat_x) * (z + mat_x) / 2 / disp / disp));
            
        }

        // Текущая координата
        double z(int i)
        {
            return i * dz - R;
        }

        //========================================Сеточная прогонка========================================//
        Complex fA(int i)
        {
            Complex c = new Complex(0, -(dt / (2 * dz * dz)));
            return c;
        }

        Complex fB(int i)
        {
            Complex c = new Complex(0, -(dt / (2 * dz * dz)));
            return c;
        }

        Complex fC(int i)
        {
            return 2 + ii * dt * u(z(i)) - fA(i) - fB(i);
        }

        Complex fD(Complex[] arr, int i)
        {
            return ((4 - fC(i)) * arr[i] - fA(i) * arr[i - 1] - fB(i) * arr[i + 1]);
        }

        Complex falfa(int i, Complex alfa_prev)
        {
            return -fA(i) / (fC(i) + alfa_prev * fB(i));
        }

        Complex fbeta(Complex[] arr, int i, Complex beta_prev, Complex alfa_prev)
        {
            return (fD(arr, i) - beta_prev * fB(i)) / (fC(i) + alfa_prev * fB(i));
        }
        //========================================Сеточная прогонка========================================//

        // Инициализация переменных
        void get_all_values()
        {
            N = Convert.ToInt16(NTB.Text);
            M = Convert.ToInt16(MTB.Text);
            idx = 0;
            idxZ = 1;

            R = Convert.ToDouble(RTB.Text);
            E = Convert.ToDouble(EnergyTB.Text);

            WFdisp = Convert.ToDouble(WFdispTB.Text);
            disp = Convert.ToDouble(dispTB.Text);
            WFmat_x = Convert.ToDouble(WFmat_xTB.Text);
            mat_x = Convert.ToDouble(mat_xTB.Text);
            V0 = Convert.ToDouble(V0TB.Text);

            dz = 2 * R / M;
            dt = Convert.ToDouble(dtTB.Text);

           
            psi = new Complex[N][];
            for (int i = 0; i < N; i++)
                psi[i] = new Complex[M + 1];
            temp_psi = new Complex[M + 1];
        }

        // Инициализация пакета
        void start_init()
        {
            // Вычислим нормировочный множитель
            WFamp = 0;
            for (int i = 0; i <= M; i++)
            {
                temp_psi[i] = new Complex(Math.Exp(-(z(i) - WFmat_x) * (z(i) - WFmat_x) / 2 / WFdisp / WFdisp), 0);
                WFamp += temp_psi[i].Real * dz;
            }

            // Вычислили нормировочный множитель, домножим на него ВФ
            for (int i = 0; i <= M; i++)
            {
                temp_psi[i] /= WFamp;
            }
            psi[0] = temp_psi;
        }

        // Расчет в таймере
        private void MainTimer_Tick(object sender, EventArgs e)
        {
            Complex[] alfa, beta;

            alfa = new Complex[M + 1];
            beta = new Complex[M + 1];

            for (int i = 0; i <= M; i++)
            {
                if (i == 0 || i == 1)
                {
                    alfa[0] = 0;
                    beta[0] = 0;
                    alfa[1] = 0;
                    beta[1] = 0;
                }

                if (i > 1 && i <= M)
                {
                    alfa[i] = falfa(i, alfa[i - 1]);
                    beta[i] = fbeta(temp_psi, i - 1, beta[i - 1], alfa[i - 1]);
                }
            }

            temp_psi[M] = 0;
            for (int i = M - 1; i > 0; i--)
            {
                temp_psi[i] = alfa[i + 1] * temp_psi[i + 1] + beta[i + 1];
            }

            draw(temp_psi, M + 1, -R, R, 1);

            if (Fourier)
            {
                if (idx < N)
                {
                    for (int i = 0; i <= M; i++)
                    {
                        psi[idx][i] = temp_psi[i];
                    }
                    idx++;
                    //FourierProgress.Value = idx;
                }
                else
                {
                    Fourier = false;
                    GetSpectreBT.Enabled = true;
                    idx = 0;
                    MainTimer.Stop();
                    StartTimerBT.Text = "Продолжить";
                    start = true;
                    MessageBox.Show("Готово");
                }
            }
        }

        // Первая кнопка
        private void InitializeBT_Click(object sender, EventArgs e)
        {
            get_all_values();
            start_init();

            first = true;
            Fourier = false;
            StartTimerBT.Text = "Запуск таймера";
            MainTimer.Stop();
            start = true;
           // FourierProgress.Value = idx;
            StartTimerBT.Enabled = true;
            WritingForFourierBT.Enabled = false;
            GetSpectreBT.Enabled = false;
            draw(psi[0], M + 1, -R, R, 1);
        }

        // Вторая кнопка
        private void StartTimerBT_Click(object sender, EventArgs e)
        {
            WritingForFourierBT.Enabled = true;
            if (start)
            {
                MainTimer.Enabled = true;
                MainTimer.Start();
                StartTimerBT.Text = "Стоп";
                start = false;
            }
            else
            {
                MainTimer.Stop();
                StartTimerBT.Text = "Продолжить";
                start = true;
            }
        }

        // Третья кнопка
        private void WritingForFourierBT_Click(object sender, EventArgs e)
        {
           // FourierProgress.Maximum = N;
           // FourierProgress.Value = 0;
            Fourier = true;
        }

        // Четвертая кнопка
        private void GetSpectreBT_Click(object sender, EventArgs e)
        {
            Form2 form = new Form2(N, M, idxZ, dt, R, psi);
            form.ShowDialog();
            if (form.DialogResult == DialogResult.OK)
            {
                Complex[] arr = form.return_WF();
                idxZ = form.return_idx();
                draw(arr, M + 1, -R, R, 1);
            }
        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void Graph_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
